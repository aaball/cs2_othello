#include "board.h"
#include <stdio.h>

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);

    // weight
    float weight = 4;

    // unweighted middle
    for(int x = 2; x < 6; x++) {
        for(int y = 2; y < 6; y++) {
            boardWeight[x + 8*y] = 0;
        }
    }

    // edge buffers and edges
    for(int i = 2; i < 6; i++) {
        // edge buffers
        boardWeight[i + 8] = -.25 * weight;
        boardWeight[55 - i] = -.25 * weight;
        boardWeight[1 + 8*i] = -.25 * weight;
        boardWeight[6 + 8*i] = -.25 * weight;

        // edges
        boardWeight[i] = 1.5 * weight;
        boardWeight[63 - i] = 1.5 * weight;
        boardWeight[8*i] = 1.5 * weight;
        boardWeight[7 + 8*i] = 1.5 * weight;
    }

    // corner buffers
    boardWeight[9] = -.5 * weight * weight;
    boardWeight[14] = -.5 * weight * weight;
    boardWeight[49] = -.5 * weight * weight;
    boardWeight[54] = -.5 * weight * weight;

    // corners
    boardWeight[0] = 5 * weight * weight;
    boardWeight[7] = 5 * weight * weight;
    boardWeight[56] = 5 * weight * weight;
    boardWeight[63] = 5 * weight * weight;

    /* The value of edge squares adjacent to corners is highly dependent on
     * context, so I assign them no default weight. wait
     */
    boardWeight[1] = -.5 * weight * weight;
    boardWeight[6] = -.5 * weight * weight;
    boardWeight[8] = -.5 * weight * weight;
    boardWeight[15] = -.5 * weight * weight;
    boardWeight[48] = -.5 * weight * weight;
    boardWeight[55] = -.5 * weight * weight;
    boardWeight[57] = -.5 * weight * weight;
    boardWeight[62] = -.5 * weight * weight;
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

/*
 * Returns true iff the square holds a piece of the given side.
 */
bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

/*
 * Does the indicated move, even if the squares were already occupied
 */
void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

/* 
 * Returns whether the given square is on the board
 */
bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    // possible rewrite. Checking both in same loop should halve time in worst case
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j); // how the hell does this work?
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) {
        fprintf(stderr, "A NULL move was just checked\n");
        return !hasMoves(side);
    }

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (taken[X + 8*Y]) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) {
                    return true;
                }
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {

    // A NULL move means pass.
    if (m == NULL) {
        return;
    }

    // Ignore if move is invalid.
    if (!checkMove(m, side)) {
        fprintf(stderr, "An invalid move was called\n");
        return;
    }

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X + dx;
            int y = Y + dy;
            while (onBoard(x, y) && get(other, x, y)) {
                x += dx;
                y += dy;
            }

            if (onBoard(x, y) && get(side, x, y)) {
                while (x != X || y != Y) {
                    x -= dx;
                    y -= dy;
                    set(side, x, y);
                }
            }
        }
    }
    set(side, X, Y);

}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

float Board::countEmpty() {
    return (float) (64 - taken.count());
}

/*
 * Calculates and returns the score heuristic for the given side, in the current state.
 */
float Board::score(Side side) {
    float score = 0;

    // vertical edges (not including corners or corner buffers)
    for(int y = 2; y < 6; y++) {
        // left edge
        if (taken[0 + 8*y]) {
            if (black[0 + 8*y]) {
                if (fxor(taken[0 + 8*(y-1)] && !black[0 + 8*(y-1)], taken[0 + 8*(y+1)] && !black[0 + 8*(y+1)])) {
                    score -= boardWeight[0 + 8*y];
                } else {
                    score += boardWeight[0 + 8*y];
                }
            } else {
                if (fxor(black[0 + 8*(y-1)], black[0 + 8*(y+1)])) {
                    score += boardWeight[0 + 8*y];
                } else {
                    score -= boardWeight[0 + 8*y];
                }
            }
        }
        // right edge
        if (taken[7 + 8*y]) {
            if (black[7 + 8*y]) {
                if (fxor(taken[7 + 8*(y-1)] && !black[7 + 8*(y-1)], taken[7 + 8*(y+1)] && !black[7 + 8*(y+1)])) {
                    score -= boardWeight[7 + 8*y];
                } else {
                    score += boardWeight[7 + 8*y];
                }
            } else {
                if (fxor(black[7 + 8*(y-1)], black[7 + 8*(y+1)])) {
                    score += boardWeight[7 + 8*y];
                } else {
                    score -= boardWeight[7 + 8*y];
                }
            }
        }
    }

    // horizontal edges (not including corners or corner buffers)
    for(int x = 2; x < 6; x++) {
        // top edge
        if (taken[x + 8*0]) {
            if (black[x + 8*0]) {
                if (fxor(taken[x-1 + 8*0] && !black[x-1 + 8*0], taken[x+1 + 8*0] && !black[x+1 + 8*0])) {
                    score -= boardWeight[x + 8*0];
                } else {
                    score += boardWeight[x + 8*0];
                }
            } else {
                if (fxor(black[x-1 + 8*0], black[x+1 + 8*0])) {
                    score += boardWeight[x + 8*0];
                } else {
                    score -= boardWeight[x + 8*0];
                }
            }
        }
        // bottom edge
        if (taken[x + 8*7]) {
            if (black[x + 8*7]) {
                if (fxor(taken[x-1 + 8*7] && !black[x-1 + 8*7], taken[x+1 + 8*7] && !black[x+1 + 8*7])) {
                    score -= boardWeight[x + 8*7];
                } else {
                    score += boardWeight[x + 8*7];
                }
            } else {
                if (fxor(black[x-1 + 8*7], black[x+1 + 8*7])) {
                    score += boardWeight[x + 8*7];
                } else {
                    score -= boardWeight[x + 8*7];
                }
            }
        }
    }

    // corners
    if (taken[0 + 8*0]) {
        if (black[0 + 8*0]) {
            score += boardWeight[0 + 8*0];
        } else {
            score -= boardWeight[0 + 8*0];
        }
    }
    if (taken[7 + 8*0]) {
        if (black[7 + 8*0]) {
            score += boardWeight[7 + 8*0];
        } else {
            score -= boardWeight[7 + 8*0];
        }
    }
    if (taken[0 + 8*7]) {
        if (black[0 + 8*7]) {
            score += boardWeight[0 + 8*7];
        } else {
            score -= boardWeight[0 + 8*7];
        }
    }
    if (taken[7 + 8*7]) {
        if (black[7 + 8*7]) {
            score += boardWeight[7 + 8*7];
        } else {
            score -= boardWeight[7 + 8*7];
        }
    }

    // corner buffers
    if (taken[1 + 8*0]) {
        if (black[1 + 8*0]) {
            if (!black[0 + 8*0]) {
                score += boardWeight[1 + 8*0]; // Notice: boardWeight is negative here, so this position is bad
            }
        } else {
            if (!taken[0 + 8*0] || black[0 + 8*0]) {
                score -= boardWeight[1 + 8*0];
            }
        }
    }
    if (taken[1 + 8*1]) {
        if (black[1 + 8*1]) {
            if (!black[0 + 8*0]) {
                score += boardWeight[1 + 8*1]; // Notice: boardWeight is negative here, so this position is bad
            }
        } else {
            if (!taken[0 + 8*0] || black[0 + 8*0]) {
                score -= boardWeight[1 + 8*1];
            }
        }
    }
    if (taken[0 + 8*1]) {
        if (black[0 + 8*1]) {
            if (!black[0 + 8*0]) {
                score += boardWeight[0 + 8*1]; // Notice: boardWeight is negative here, so this position is bad
            }
        } else {
            if (!taken[0 + 8*0] || black[0 + 8*0]) {
                score -= boardWeight[0 + 8*1];
            }
        }
    }

    if (taken[0 + 8*6]) {
        if (black[0 + 8*6]) {
            if (!black[0 + 8*7]) {
                score += boardWeight[0 + 8*6]; // Notice: boardWeight is negative here, so this position is bad
            }
        } else {
            if (!taken[0 + 8*7] || black[0 + 8*7]) {
                score -= boardWeight[0 + 8*6];
            }
        }
    }
    if (taken[1 + 8*6]) {
        if (black[1 + 8*6]) {
            if (!black[0 + 8*7]) {
                score += boardWeight[1 + 8*6]; // Notice: boardWeight is negative here, so this position is bad
            }
        } else {
            if (!taken[0 + 8*7] || black[0 + 8*7]) {
                score -= boardWeight[1 + 8*6];
            }
        }
    }
    if (taken[1 + 8*7]) {
        if (black[1 + 8*7]) {
            if (!black[0 + 8*7]) {
                score += boardWeight[1 + 8*7]; // Notice: boardWeight is negative here, so this position is bad
            }
        } else {
            if (!taken[0 + 8*7] || black[0 + 8*7]) {
                score -= boardWeight[1 + 8*7];
            }
        }
    }

    if (taken[6 + 8*7]) {
        if (black[6 + 8*7]) {
            if (!black[7 + 8*7]) {
                score += boardWeight[6 + 8*7]; // Notice: boardWeight is negative here, so this position is bad
            }
        } else {
            if (!taken[7 + 8*7] || black[7 + 8*7]) {
                score -= boardWeight[6 + 8*7];
            }
        }
    }
    if (taken[6 + 8*6]) {
        if (black[6 + 8*6]) {
            if (!black[7 + 8*7]) {
                score += boardWeight[6 + 8*6]; // Notice: boardWeight is negative here, so this position is bad
            }
        } else {
            if (!taken[7 + 8*7] || black[7 + 8*7]) {
                score -= boardWeight[6 + 8*6];
            }
        }
    }
    if (taken[7 + 8*6]) {
        if (black[7 + 8*6]) {
            if (!black[7 + 8*7]) {
                score += boardWeight[7 + 8*6]; // Notice: boardWeight is negative here, so this position is bad
            }
        } else {
            if (!taken[7 + 8*7] || black[7 + 8*7]) {
                score -= boardWeight[7 + 8*6];
            }
        }
    }

    if (taken[7 + 8*1]) {
        if (black[7 + 8*1]) {
            if (!black[7 + 8*0]) {
                score += boardWeight[7 + 8*1]; // Notice: boardWeight is negative here, so this position is bad
            }
        } else {
            if (!taken[7 + 8*0] || black[7 + 8*0]) {
                score -= boardWeight[7 + 8*1];
            }
        }
    }
    if (taken[6 + 8*1]) {
        if (black[6 + 8*1]) {
            if (!black[7 + 8*0]) {
                score += boardWeight[6 + 8*1]; // Notice: boardWeight is negative here, so this position is bad
            }
        } else {
            if (!taken[7 + 8*0] || black[7 + 8*0]) {
                score -= boardWeight[6 + 8*1];
            }
        }
    }
    if (taken[6 + 8*0]) {
        if (black[6 + 8*0]) {
            if (!black[7 + 8*0]) {
                score += boardWeight[6 + 8*0]; // Notice: boardWeight is negative here, so this position is bad
            }
        } else {
            if (!taken[7 + 8*0] || black[7 + 8*0]) {
                score -= boardWeight[6 + 8*0];
            }
        }
    }

    // edge buffers
    for(int i = 2; i < 6; i++) {
        if (taken[i + 8*2]) {
            if (black[i + 8*2]) {
                score += boardWeight[i + 8*2];
            } else {
                score -= boardWeight[i + 8*2];
            }
        }
        if (taken[i + 8*5]) {
            if (black[i + 8*5]) {
                score += boardWeight[i + 8*5];
            } else {
                score -= boardWeight[i + 8*5];
            }
        }
        if (taken[2 + 8*i]) {
            if (black[2 + 8*i]) {
                score += boardWeight[2 + 8*i];
            } else {
                score -= boardWeight[2 + 8*i];
            }
        }
        if (taken[5 + 8*i]) {
            if (black[5 + 8*i]) {
                score += boardWeight[5 + 8*i];
            } else {
                score -= boardWeight[5 + 8*i];
            }
        }
    }

    // unit scoring
    for(int x = 0; x < 8; x++) {
        for(int y = 0; y < 8; y++) {
            if (taken[x + 8*y]) {
                if (black[x + 8*y]) {
                    score++;
                } else {
                    score--;
                }
            }
        }
    }

    if (side == WHITE) {
        score = -score;
    }
    return score;

}

/*
 * Calculates and returns a simple score heuristic for the given side.
 */
float Board::simpleScore(Side side) {
    /*
     * Calculates the score for black, and then
     * takes the negative if side == WHITE
     */
    float score = 0;
    float scale = 1.0 - taken.count()*taken.count() / 4096.0;

    for(int i = 0; i < 64; i++) {
        if (taken[i]) {
            if (black[i]) {
                score += 1 + scale * boardWeight[i];
            } else {
                score -= 1 + scale * boardWeight[i];
            }
        }
    }

    if (side == WHITE) score = -score;

    return score;
}

bool Board::fxor(bool a, bool b) {
    if ( (a && b) || (!a && !b) ) {
        return false;
    }
    return true;
}