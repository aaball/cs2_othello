#include "exampleplayer.h"
#include <stdio.h>


/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {

    bd = Board();
    this->side = side;
    other = (side == BLACK) ? WHITE : BLACK;

}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * Update board and variables.
     */
    bd.doMove(opponentsMove, other);

    Move *bmove = new Move(0, 0);
    float bscore = -10000;

    for(int x = 0; x < 8; x++) {
        for(int y = 0; y < 8; y++) {
            Move m(x, y);
            if (bd.checkMove(&m, side)) {
                Board* copy = bd.copy();
                copy->doMove(&m, side);
                float score = copy->score(side);
                delete copy;
                if (score > bscore) {
                    bscore = score;
                    bmove->x = m.x;
                    bmove->y = m.y;
                }
            }
        }
    }

    if (bscore > -10000) {
        bd.doMove(bmove, side);
        return bmove;
    }

    return NULL;

}